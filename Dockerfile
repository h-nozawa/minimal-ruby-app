FROM ruby:latest
ADD ./ /app/
WORKDIR /app

COPY Gemfile ${APP_ROOT}/Gemfile
COPY Gemfile.lock ${APP_ROOT}/Gemfile.lock
RUN bundle install

ENV PORT 5000
EXPOSE 5000


ENTRYPOINT ["rails", "s", "-b", "0.0.0.0"]
